<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language ?>" xml:lang="<?php print $language ?>">

<head>
  <title><?php print $head_title ?></title>
  <?php print $head ?>
  <?php print $styles ?>
  <?php print $scripts ?>
  <script type="text/javascript"><?php /* Needed to avoid Flash of Unstyle Content in IE */ ?> </script>
</head>

<body>
	<div class="content">
        <div class="top_header"><?php print $header ?></div>

        <div class="header_right">
			<div class="top_info">
				<div class="top_info_right">
                    <p>
<?php
global $user;
if ($user->uid) {
    print t('<b>You are logged in as !name', array('!name' => theme('username', $user))) .'</b> | '. l(t('Log out'), 'logout');
}
else {
    print t('<b>You are not Logged in!</b> ' . l("Log in", "user/login", array(), drupal_get_destination()) . ' or ' . l("register", "user/register") . '.');
}
?>                    
                    </p>
				</div>		
			</div>
					
			<div class="bar">
                <?php if (isset($primary_links)) { ?><?php print theme('links', $primary_links) ?><?php } ?>
			</div>
		</div>
		
<?php if ($logo || $site_name) { ?>        
		<div class="logo">
            <h1><a href="<?php print $base_path ?>" title="<?php print check_plain($site_name) ?>"><img src="<?php print $logo ?>" alt="<?php print check_plain($site_name) ?>" /><? print check_plain($site_name) ?></a></h1>
            <?php if ($site_slogan) { ?><p><span class='site-slogan'><?php print $site_slogan ?></span></p><?php } ?>
		</div>
<?php } ?>
		
		<div class="search_field">
            <?php if ($search_box) { print $search_box; } ?>
		</div>
		
		<div class="newsletter">
			<p>Subscribe for Newsletter!</p>
		</div>
		
		<div class="subheader">
            <?php if ($mission) { ?><div id="mission"><?php print $mission ?></div><?php } ?>
            <?php print $breadcrumb ?>
        </div>
		
		<div class="left">
            <?php if ($content_top) { ?>
            <div class="content_top">
                <?php print $content_top ?>
            </div>
            <?php } ?>
			<div class="left_articles">
                <?php if ($title) { ?>
                <h1 class="title"><?php print $title ?></h1>
                <?php } ?>
                <?php if ($tabs) { ?>
                <div class="tabs"><?php print $tabs ?></div>
                <?php } ?>
                <?php print $help ?>
                <?php print $messages ?>
                <?php print $content; ?>
			</div>
            <?php if ($content_bottom) { ?>
            <div class="content_bottom">
                <?php print $content_bottom ?>
            </div>
            <?php } ?>
		</div>	
		<div class="right">
            <?php if ($sidebar_plain) print $sidebar_plain ?>
            <?php if ($sidebar_right) print $sidebar_right ?>
		</div>	
		<div class="footer">
			<?php if ($feed_icons) print $feed_icons; ?>
            <?php print $footer_message ?>
		</div>
	</div>
 <?php print $closure ?>
</body>
</html>
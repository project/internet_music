
About Internet Music
--------------------

This is a port of the Internet_Music theme by Luka Cvrk. This is a fixed width, 2 column layout.

Added
- Regions: header, content top (grey box), content, content bottom (grey box), footer, right plain, right sidebar
- Plain side block (right plain region) on top of the orange side block (right sidebar)
- Table CSS from garland
- Breadcrumb on below the search bar

Internet_Music requires the PHPTemplate theme engine. It is tested under Drupal 5.x, and valid XHTML 1.0 Strict.

Official Internet_Music theme project page:
  http://www.solucija.com/templates/demo/Internet_Music/


Ported by CK Ng <niceckng@gmail.com>
Sponsored by myFineJob.com

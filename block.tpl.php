<?php if ($block->region == "sidebar_right") { ?>
<div class="rt"></div>
<div class="right_articles">
<?php } else if ($block->region == "sidebar_plain") { ?>
<div class="right_plain">
<?php } else if ($block->region == "content_top" || $block->region == "content_bottom") { ?>
<div class="lt"></div>
<div class="lbox">
<?php } else { ?>
<div>
<?php } ?>
  <div class="block block-<?php print $block->module; ?>" id="block-<?php print $block->module; ?>-<?php print $block->delta; ?>">
    <h2 class="title"><?php print $block->subject; ?></h2>
    <div class="content"><?php print $block->content; ?></div>
  </div>
</div>
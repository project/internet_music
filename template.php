<?php

function phptemplate_search_theme_form($form) {
  return _phptemplate_callback('search-theme-form', array('form' => $form));
}

function internet_music_regions() {
  return array(
    'sidebar_plain' => t('right plain'),
    'sidebar_right' => t('right sidebar'),
    'content_top' => t('content top'),
    'content' => t('content'),
    'content_bottom' => t('content bottom'),
    'header' => t('header'),
    'footer' => t('footer'),
  );
}
